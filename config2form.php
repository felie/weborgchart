<style>
table,tr,th,td {
    border:1px solid black;
    border-collapse:collapse;
    }
.button {
  text-transform: uppercase;
  padding: 5px;
  color: blue;
  font-weight: 700;
}
</style>
<?php

function presence($liste,$s){ // if any element of $liste is present in $s, return true, else return false
    $result=0;
    foreach ($liste as $e){
        //echo "$e dans $s ---> ".(strpos($s,$e)!== FALSE);
        $result=$result + (strpos($s,$e)!== FALSE);
        }
    //echo " $result<br>";
    return $result;
    }

function view($x,$texte=''){
        echo "<b>$texte</b><pre>";
        print_r($x);
        echo "</pre>";
    }
    
$file=$_GET['file'];
//view($_POST);
if (isset($_POST['submit']) and $_POST['submit']=='OK')
    {
    $file=$_POST['FormFile'];
    $r='';
    for($i=0;$i<$_POST['nblines'];$i++){
        //echo "$i ";
        if (isset($_POST["line$i"])) // category 
            $r.="\n".$_POST["line$i"]."\n"; 
        else // variable
           // foreach ($_POST["var$i"] as $key=>$value)
                {
                $key=$_POST['name'][$i];
                $type=$_POST['type'][$key];
                //echo "$i name=$key $type<br>";
                if (isset($_POST["var$i"]))
                    if ($type=='checkbox')
                        $value=1;
                    else
                        $value=$_POST["var$i"][$key];
                else
                    $value=0;
                if (presence([';','"',"'",'<','>'],$value)){
                    echo "EMERGENCY: CODE INJECTION";
                    exit;
                    }
                //echo "value($key)=$value<br>";
                if ($type=='text' or $type=='hidden' or $type=='select')
                    $value='"'.$value.'"';
                $r.='$'.$key."=$value;//".$_POST['type'][$key].'('.$_POST['default'][$key].') '.trim($_POST['comment'][$key])."\n";
                }
        }
    //echo "<pre>$r</pre>";
    file_put_contents($file,"<?php\n$r?>");
    }

$t=explode("\n",file_get_contents($file));
array_pop($t); // remove <?php and end
array_shift($t); 
$r="<form method='POST'>
    <table>
    <tr>
     <th>Variable</th>
     <th>Valeur</th>
     <th>Default</th>
     <th>Commentaire</th>
    </tr>

";
    
$n=0;
foreach($t as $l){
    if (preg_match('/^\$(.*)=[\"\']?(.*)[\"\']?;\/\/([^\(]*)\(?([^\)]*)\)?(.*)$/',$l,$m)){
        //echo "$n<br>";view($m);
        $type=$m[3];
        $value=str_replace(['"',"'"],['',''],$m[2]);
        $default=str_replace(['"',"'"],['',''],$m[4]);
        if ($type=='checkbox' and $m[2]==1)
            $checked='checked';
        else
            $checked='';
        if ($type=='select'){
            $options=explode(',',$m[4]);
            $choice="<select name='var$n"."[".$m[1]."]'>";
            foreach ($options as $o)
                $choice.="<option value='$o'>$o</option>";
            $choice.='</select>';
            }
        else
            $choice="<input name='var$n"."[".$m[1]."]' value='$value' type='$m[3]' $checked>";
        $r.="<tr>
              <td align=right>$m[1]</td>
              <td>$choice</td>
                <input type=hidden name='name[$n]' value='$m[1]'>
                <input type=hidden name='type[$m[1]]' value='$m[3]'>
                <input type=hidden name='default[$m[1]]' value='$m[4]'>
                <input type=hidden name='comment[$m[1]]' value='$m[5]'>
              <td>$default</td>
              <td>$m[5]</td>
            </tr>";
        }
    else
        if (preg_match('|^// (.*)$|',$l,$m)){
            $r.="<tr>
                  <td colspan=4 style='background-color:lightgreen;'><b>$m[1]</b><input type=hidden name='line$n' value='// $m[1]'></td>
                 </tr>";
        }
    if (trim($l)!='')
        $n++;
    //echo "$n *$l*<br>";
    }
    $n--;
$r.="<tr>
      <td colspan=3></td>
      <input type=hidden name=nblines value=$n>
      <td align='right'><input type=hidden name='FormFile' value='$file'><input name='submit' class=button type='submit' value='OK'></td>
     </tr>
     </table>
     </form>";
echo $r;
?>
