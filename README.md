# WebOrgChart[^1]

## En un mot

À partir d'un simple fichier texte (une liste de liens hiérarchiques n / n+1 dans le répetoire *orgas*), WOC produit un pdf multipage d'un organigramme en tenant compte:
- d'un fichier de configuration d'options fines (*config.php*)
- d'un fichier d'*ordres*, indiquant d'afficher une page par direction par exemple (dans le répertoire *orders*)
- d'un *thème* (décrivant les manières d'afficher les niveaux)[^2] (dans le répertoire *themes*)

Qui peut le plus peut le moins: WOC produit évidemment le même organigramme, en tout ou partie, en html dans le navigateur.

Voici un exemple d'organigramme:

![img](./documentation/images/sample.png "sample")

Produit automatiquement à partir de la liste hiérarchique suivante:

<small><small>

    DP = direction/Direction de la production/Yves Atrovite/ -- DG = top/Exemple S.A./Bruno Dajin/A
    service/Logistique/Camille Honnête/A -- DP
    TA = service/Usine /Thor Aipaleur/A -- DP
    Mona Ster -- TA
    DF = direction/Direction financière/Alex Térieur/ -N DG
    service/Paie/Alain Daixe/ -N DF
    service/Comptabilité/Sally Stoir/A -N DF
    service/Contrôle de gestion/Jeanne Hultout/B -N DF
    RH = direction/Direction des ressources humaines/Iris Keurien/ -N DG
    SA = Sam Agass -A RH
    RC = service/Recrutement/Homère Dalor/ -N RH
    Ali Gator -- SA
    service/Gestion du personnel/Samira Traibien/ -N RH
    DC = direction/Direction commerciale/The vendor/ -n DG
    SAV = service/Service après vente/Paula Risée/ -N DC
    service/Marketing/Nestor Vénitien/ -N DC
    P = service/Produit/Maggie Mauve/ -N DC
    DA = direction/Direction Adm. et Juridique/Marc Assin/ -N DG
    service/Aministration/Marie Toisituveuxpayermoinsdimpot/ -N DA
    service/Audit interne/Leslie Pancuir/ -N DA
    service/Service juridique/Judas Briqueau/ -N DA
    LO = mission/Secrétariat Général/Laurent Outan/ -R DG
    Mort Adèle -- LO 
    EG = mission/secrétariat Finances/Elmer Gaize/ -B DF
    //Marina Voile/ -- EG
    PO = mission/Contrôleur de gestion/Paul Ochon/ -L DF
    Médhi Caman -- PO
    mission/Secrétariat Commercial/Mel Odrame/ -B DC
    
</small></small>

## Vocabulaire

- *theme*: une liste de niveaux d'étiquettes (couleur, largeur, hauteur, taille de police)
- *orga*: une liste de relations hiérarchiques (n / n+1)
- *ordre*: une liste d'ordre pour produire le document

## Éditeur de thème

WOC founit un éditeur de thème (*theme_ma

## Syntaxe des relations hiérarchiques (orgas)

Les lignes du fichier doivent être de la forme

    [code = ][niveau/fonction/]nom[/categorie] <relation> [code = ][niveau/fonction/]nom[/categorie]

exemple (les codes de définitions sont facultatifs mais pratiques)

    MM = assistant/Bras droit/Mickey Mouse/B -- P = ceo/Grand Sachem/Picsou/A

MM et P peuvent être utilisés par la suite, par exemple:

    riri -- MM

### Relations hiérarchiques

- sur le côté
  - à gauche **-L**
  - à droite **-R**
- dessous sur le côté
  - à gauche **-A**
  - à droite **-B**
- dessous **--*ù* ou **-N**

Lien en pointillé: mettre la lettre en minuscule.

[Voir en détail](documentation/relations.md)

# Step by step

## Links

The link are defined like this

    item link item

### Abreviation

An item can be abreviate.

    CEO = Chief Executive Officer
    
You can use CEO after like this

    Agent -- CEO
    
### Items

Persons in the organization are defined like this:

    <status>/<position displayed>/<name>/<category>
    
While it is mandatory, you can tell only name, like this:

    //name/
    
#### The status ? For the type of cards!
    
![screenshot](images/position1.png "Default")

The status can define the colors and the size of the card.

There is now an editor of profiles (12 items in a profile)
    
### Hierarchical link
    
    B -- A
    
![screenshot](images/link1.png "Default")

    
B will be displayed under A

### Lateral links

    E -- A
    C -l A
    F -l A
    D -r A    
    
![screenshot](images/link2.png "Default")

Notice, you should have an item under A Otherwise, lateral links cannot exist. 
    
## Fonctional links

You can define free relations between persons in the data file with the relations -0- and -1-

	Person 1 -0- Persons 2
	Person 8 -1- CEO

-0- indicates: no dashed line
-1- indicates: dashed line

# Convertisseur du html vers pdf: wkhtmltopdf

J'ai testé:
- jspdf (outil client): pas de rendu des lignes en pointillées, multipages pas simple du tout pour du html propre en pdf (il faudrait se contenter d'images) 
- weasyprint (outil serveur en python): mauvais rendu des largeurs pour les tableaux.
Je me suis arrété à:
- wkhtmltopdf: très bon rendu html et svg. Reste à régler un problème pour les liens internes.

# Vous voulez une démo

Voir ici: https://elie.org/weborgchart

Cette démo est aussi un site gratuit de générateur d'organigramme qui peut très bien suffire pour de petites structures: on fournit une orga, un fichier d'ordre et WOC produit le pdf.

# License
- WebOrgChart est sous licence GPLv3 Affero
## Composants utilisés
- Leader-line d'anseki (pour les tracés de lignes) est sous licence MIT
- html2canvas de Niklas von Hertzen (pour la conversion de tables en image) est sous licence MIT

[^1]: Reconnaissons une dette. Après avoir considéré graphviz et Tikz comme outil pour présenter graphiquement des organigrammes, PHP-to-OrgChart (https://github.com/Awezome/PHP-to-OrgChart) m'a donné l'idée de faire en html. J'ai ajouté de la récursivité et d'autres liens hiérarchiques (latéraux), et des relations fonctionnelles entre les personnes, figurées par des lignes libres. Pour ces différentes raisons, WOC n'est pas un fork de PHP-to-OrgChart, mais un projet tout à fait différent, notamment dans son ambition.

[^2]: WOC intègre un éditeur de thèmes.

