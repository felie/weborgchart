<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="modal.css">
</head>
<body> 
<script>
document.onkeydown = function(e) {
  e = e || window.event;
  if ("key" in e ? e.key === "Escape" || e.key === "Esc" : e.keyCode === 27)
    location.href = "#ok"; // any unused achor is #ok.
}
function appel_ajax(target,content)
{
    var xhr=null;
    xhr = new XMLHttpRequest()

    if (window.XMLHttpRequest) { 
        xhr = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) 
    {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhr.open("POST", "answer.php", false);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("content="+encodeURI(content));
    document.getElementById(target).innerHTML=xhr.responseText;
}
</script>



<div class="box">
	<a href="#popup1" onclick="appel_ajax('modalcontent','coco.html');">Let me Pop up</a>
</div>

<div id="popup1" class="overlay">
	<div class="popup">
		<a class="close" href="#">&times;</a>
		<div id=modalcontent class="content">
			Thank to pop me out of that button, but now i'm done so you can close this window.
		</div>
	</div>
</div>

<p>Popup is closeable on ESC key.</p>

</body>
</html>
