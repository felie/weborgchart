<?php

// option for labels

$shadow=1;
$roundborder=4; // 0 or a number for the roundborder

$labelwidth=164; // px 
$variablewidth=1; // to fix the with of all labels to $labelwidth
$labelheight=16; // px
$labelfontsize=14; // px
$upcasefirstname=1; // default: 1

$labelbackgroundcolor='lightgreen';
$labelpencolor='black';

// category on label

$categories_sum=0;
$category_in_label=0;

// disposition

$lateral_links=1;   // with 0, rake for all, except blocks (-r and -l are interpreted as --)

$block_mode=1; // with 0 if all son haven't sons, no rake: a block_mode
$compact_blocks=1; // blocks are condensed in one label with all names in it
$lateral_child=0; // alternative for blocks display

$liaisons=0; // free links solid or dotted between labels

$lateraldecal=15;
$valign='middle'; // bottom or top or middle (vertical alignment for the lateral relatives) default: middle, bottom if ther are lateral relatives for those who have no sons below
$hook_height=20;    // height of the hook in the rake (in px)
$vspace=7;         // vertical space between elements
$sepInBlock=10;      // vertical space between blocks element
$Margin=15;         // horizontal space between element in a rake

$thickness=1;  // 1=2px (thickness of the lines between labels) 

$backgroundcolor='transparent';//'ivory';

$flex=0; // centered lateral links (-r and 

// pdf
$produce_pdf=0;
$orientation='a4l'; // a4l (landscape) or a4p (portrait) 

// for debugging

$debug=0;
$aleaLevelForLetters=0; // a random level for single lettres: to test disposition of the label0

// do not touch

$default_level='default;#3465a4;#ffffff;14;160;20';
$bloc_level='bloc;#3465a4;#ffffff;14;160;0'; // height of 0!

// strings

$orga='angouleme';

$PDF=
'direction*->4
service*->2
XB->10000
NE->2';

$HTML=
'XB -1';

?>
