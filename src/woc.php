<?php
/*
    Licenced GPLv2
    francois@elie.org 2022 
*/
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED);
ini_set("display_errors", 1);

include_once 'get_data.php';
include_once 'profiles_management.php';
include_once 'style.php';
include_once 'util.php';


if ($debug)
    echo "<h1>mode debug activé</h1><br>";

$left=[];
$right=[];
$def=[];
$relations=[];
$link=[];
$cat=[];

    function cats($c,$hiscat){
        global $category_in_label;
        $r='';
        if (!isset($c['A']))
            $c['A']=0;
        if (!isset($c['B']))
            $c['B']=0;
        if (!isset($c['C']))
            $c['C']=0;
        $r='';
        if ($c['A']+$c['B']+$c['C']>0)
            $r.= "<div class='cat catA'>".$c['A']."</div><div class='cat catB'>".$c['B']."</div><div class='cat catC'>".$c['C']."</div>";
        if ($category_in_label and $hiscat!='')
            $r.="<div class='cat hiscat'>$hiscat</div>";
        return $r;
        }
        
// version 2

$shadows='';
if ($shadow)
$shadows="-webkit-box-shadow: 2px 2px 11px 1px #000000; 
box-shadow: 2px 2px 11px 1px #000000;";

function alea(){
    return mt_rand(10,90).'px ';
    }
    
function nameOf($s){
    global $def;
    if (isset($def[$s]))
        $s=$def[$s];
    $s=explode('/',$s);
    $s=trim($s[2]);
    if ($s=='')
        $s='x';
    return $s;
    }

function node($from,$s)
        {
        global $def,$cat,$relations,$categories_sum,$labelWidth,$labelheight,$shadows,$aleaborder,$profile_height,$profile_width,$profile_list,$debug,$Margin,$lateral_child,$oklateral,$lateraldecal,$hook_height,$sepInBlock,$thickness;
        $title=$s;
        if (isset($def[$s])){
            //echo "def de $s<br>";
            $id=" id='$s'"; // token is taken for id
            $s=$def[$s];
            }
        $pos=strpos($s,'/');
        if ($pos===false)
            $s="default//$s/";
        //echo "s=$s<br>";
        $s=str_replace(', ','<br>',$s); // several persons
        $t=explode('/',$s);
        if (!isset($id))
            $id=" id='$s'" ;
        $t[0]=strtolower($t[0]); // for the profile statut
        $t[0]=profileOf($s); 
        if ($t[0]=='')
            $t[0]='default';
        if ($t[1]=='' and $t[0]!='bloc')
            $t[1]='function';
        if ($debug)
            $t[1]=$t[0];//.' cible='.floor($profile_width[$t[0]]/2)+$Margin.'* '.hspace($s);
        $t[0].='_level';
        if ($t[2]=='')
            $t[2]='x';
        if ($categories_sum)
            $categories=cats($cat[$title],$t[3]);
        //if ($categories!='')
          //  $categories="<br>$categories";
        $hh=$profile_height[profileOf($s)];
        if (!$lateral_child or !$oklateral)
            $r.="<div $id class='$t[0] radius1' style='overflow:visible;position:relative;z-index:1;text-align:center;$shadows;'>
                    <div style='min-height:$hh"."px'>$t[1]</div>
                <div class='label radius2' style='overflow:visible;text-align:center;vertical-align:bottom;'>
                $t[2]$categories</div></div>";
                
        else {
            $w=$profile_width[profileOf($s)];
            $dw=floor($w/2);
            $r.="<div style='padding-left:$thickness"."px'><div class='left bottom' style='margin-left:-".($dw)."px'>&nbsp;<div $id class='$t[0] radius1' style='margin-bottom:-$labelheight"."px;margin-left:$lateraldecal"."px;position:relative;z-index:1;display:table;text-align:center;$shadows;'>
                    <div style='position:relative;overflow:hidden;min-height:$hh"."px'>$t[1]</div>
                <div class='label radius2' style='vertical-align:bottom;'>
                    $t[2]
                </div>
                </div>
                <div style='z-index:2'>".$categories."</div></div></div>";
            }
        return $r;
        }
        
    
$fin='0.001em';
$heightpx=$vspace.'px';
$hookHeight=$hook_height.'px';
$labelWidth=$labelwidth.'px';
$labelWidthMarged=($labelwidth+$Margin).'px';
//$label3=floor($labelwidth/2+$Margin).'px';
$halflabel=floor($labelwidth / 2);
$halfLabel=$halflabel.'px';
$Marginpx=$Margin.'px';
//$MarginLpx=($Margin+floor($labelwidth / 2)).'px';
//$halfLabelMarged=(floor($labelwidth / 2)+$Margin).'px';
function th($link){
    global $thick;
    $result='';
    if (!$thick["$link"])
        $result='dotted';
    return $r;
    }
    
function leftHook($th){
    global $hookHeight;
    return "<tr><td style='vertical-align:top;width:100%' class='right$th'></td><td class='top$th' style='width:100%;height:$hookHeight'></td></tr>";
    }
    
function simpleHook($th){
    global $hookHeight;
    echo "$th dans simplehook <br>";
    return "<tr><td class='right$th' style='vertical-align:top$th;height:$hookHeight'></td></tr>";
    }

$noHook="<tr><td style='vertical-align:top;height:$hookHeight'></td></tr>";

function rightHook($th){
    global $hookHeight;
    return "<tr class=ver><td class='top$th right$th' style='vertical-align:top;width:100%;height:$hookHeight'></td><td></td></tr>";
    }

function topHook($th){
    global $hookHeight,$Marginpx;
    return "<tr><td class='top right$th ver' style='vertical-align:top;height:$hookHeight'><div style='padding-right:$Marginpx;'></div></td><td class='top'></td></tr>";
    }
    
function hookHook($th,$e){
    return lline($e,$th);
    }


function vspace($v,$class){
    return "<tr><td class='$class'  ><div style='height:".$v."px'></div></td><td></td></tr>";
    }

function son($node,$hook,$onlylines,$depth){
    if (is_string($node))
        return chart('',$node,$hook,'','',$onlylines,$depth);
    else
        foreach($node as $key => $value) // one array only
            return chart('',$key,$hook,'','',$onlylines,$depth);
    }
    
function sons($from,$a,$depth){
    global $left,$right,$backgroundcolor,$rightHook,$topHook,$simpleHook,$labelWidthMarged,$sepInBlock,$labelwidth,$block_mode,$compact_blocks,$halfLabel,$halfLabelMarged,$Marginpx,$line,$hook_height,$thick,$under,$lateral_child,$oklateral;
    $bloc=true;
    if ($block_mode){
        foreach ($a as $n){
            if (!is_string($n))
                $bloc=false;
            else
                if (isset($left[$n]) or isset($right[$n]))
                    $bloc=false;
                }
            }
    else
        $bloc=false;
    $r=[];
    $n=0;
    if ($bloc){
        if ($lateral_child and !isset($left[$from]) and !isset($right[$from]))
            $oklateral=1;
        foreach ($a as $e){
            if ($compact_blocks)
                $r[]=str_replace("'",'&#39;',nameOf($e)); // then symbol ' break css instructions
            else
                $r[]=chart($from,$e,'','','',1,$depth);
            if ($n==0)
                $first=$e;
            $n++;
            }
        if ($compact_blocks)
            {
            $e='bloc//'.implode("<hr style=\"border: none; margin-top:1px;margin-bottom:1px;border-bottom: 1px solid black;\">",$r).'/';
            
            $r=[chart($from,$e,'','','',1,$depth)];
            }
        //$r=implode("<br><div style='margin-bottom:".(-20+$sepInBlock)."px'></div>",$r);
        if (!$oklateral)
            $r=vspace($hook_height,'right'.$thick["$from--$first"]).implode(vspace($sepInBlock,''),$r);
        else
            $r=implode('',$r);
        $oklateral=0;
        return "<tr><td></td><td>$r</td></tr>";
        }
    else{
        $nb=sizeof($a);
        $lefthand=floor($nb/2);
        $n=0;
        $r='';
        foreach ($a as $e){
            if ($nb==1){ // probably useless
                $rs=son($e,simpleHook($thick["$from--$e"]),1,$depth); // one son only
                }
            else
                if ($n==0){
                    $f=$e;
                    if (is_array($e))
                         $f=array_keys($e)[0];
                    $rs=son($e,leftHook($thick["$from--$f"]),0,$depth);
                    $first=$e;
                    }
                else
                    if ($n==($nb-1)){
                        $f=$e;
                        if (is_array($e))
                            $f=array_keys($e)[0];
                        $rs=son($e,rightHook($thick["$from--$f"]),0,$depth);
                        }
                    else
                        $rs=son($e,topHook($thick["$from--$e"]),0,$depth);
            if ($nb==1)
                return "$line $rs"; // case of on son only
            $rs="<td>$rs</td>";
            $n++;
            $r.=$rs;
            if ($n==$lefthand) // $trick to be sure the father is connected to rake (the line is divides in two table-columns)
                $r="$r
  </tr>
 </table>
</td>
<td align=left style='vertical-align:top;'>
  <table class=gree>
  <tr style='vertical-align:top;'>";
            }
        $r=
        vspace($hook_height,'right')."
 <tr class=orang><td align=right style='vertical-align:top;'>
 <table class=blu>
 <tr style='vertical-align:top;'>$r
 </tr>
 </table></td></tr>";
        }
    return $r;
    }
    
function profileOf($value){
    global $variablewidth,$profile_width,$def,$Margin,$profile_list,$debug;
    if (isset($def[$value]))
        $value=$def[$value];
    preg_match('|^(.*)/(.*)/(.*)/(.*)$|',$value,$m);
    if ($m!=[]){
        $r=$profile=strtolower($m[1]);
        if ($r=='')
            $r='default';
        if (!in_array($r,$profile_list)){
            echo "ProfileOf dit: profil <b>$r</b>de label non connu</br>";
            $r='default';
            }
        }
    else{
        $r='default';
        if ($debug)
            echo "ProfileOf dit: souci de syntaxe sur la line<b>$value</b> ../../../.. <br>";
        }
    //echo $r;
    return $r;
    }
    
function correction_height($node){
    global $profile_height,$labelheight;
    return floor(($profile_height[profileOf($node)]+$labelheight)/2);
    }
    
function left($from,$a){
    global $noHook,$thick;
    if ($a=='')
        return '';
    $r=[];
    foreach ($a as $key => $value)
        $r[]=chart($from,$value,$noHook,'',lline($value,$thick["$from--$value"]));
    return implode($r);
    }
    
function right($from,$a){
    global $noHook,$thick;
    if ($a=='')
        return '';
    $r=[];
    foreach ($a as $key => $value)
        $r[]=chart($from,$value,$noHook,lline($value,$thick["$from--$value"]));
    return implode($r);
    }

define('hspace',"<div style='margin-top:-10px;height:10px;margin-left:0'></div>");

function lline($node,$th,$position='relative'){
    global $thickness,$thick;
    if ($th=='')
        $th='solid';
    return "<div style='position:$position;margin-top:-".correction_height($node)."px;height:0px;overflow:hidden;z-index:0;outline:".$thickness."px $th black;width:100%'></div>";
    }

function hspace($node,$side){
    global $profile_width,$Marginpx;
    $w=floor($profile_width[profileOf($node)]/2)+$Margin.'px';
    if ($side=='left')
        $p="padding-right:$Marginpx";
    if ($side=='right')
        $p="padding-left:$Marginpx";
    return "style='vertical-align:top;margin-top:-10px;$p;height:10px;min-width:$w'";
    }

define('vspace',"<tr><td style='height:$vspace"."px'></td><td></td></tr>");

 function chart($from,$node,$hook=vspace,$lhook='',$rhook='',$onlylines=0,$depth=0){
    global $relations,$left,$noHook,$leftHook,$rightHook,$right,$lateralleft,$lateralright,$halfLabelMarged,$Margin,$Marginpx,$labelheight,$hook_height,$thicknesspx,$backgroundcolor,$line,$valign,$profile_width,$profile_height,$flex,$thick,$oklateral,$stock;
    //echo "depth=$depth<br>";
    $p=profileOf($node);
    $pw=floor($profile_width[$p]);
    $h=floor($profile_height[$p]);
    $dw=floor($pw/2);
    $w=($dw+$Margin).'px';
    $w2=($dw+4*$Margin).'px';
    $r='';
    //echo "-> ".$thick["$node--".$lateralright[$node]]."<br>";
    if (isset($lateralleft[$node]))
        $lleft="<div style='margin-top:-".($profile_height[profileOf($lateralleft[$node])]+2*$labelheight)."px;padding-right:$dw"."px'>".chart($from,$lateralleft[$node],'','',lline($lateralleft[$node],$thick["$node--".$lateralleft[$node]]))."</div>";
    if (isset($lateralright[$node]))
        $lright="<div style='margin-top:-".($profile_height[profileOf($lateralright[$node])]+2*$labelheight)."px;padding-left:$dw"."px'>".chart($from,$lateralright[$node],'',lline($lateralright[$node],$thick["$node--".$lateralright[$node]]))."</div>";
    if (!$oklateral)
        $classright='right';
    $r.="
 $hook
 <tr style='vertical-align:top'>
  <td class='rose $classright' align=right ".hspace($node,'right')."></td>
  <td class='bleu' ".hspace($node,'left').">".node($from,$node)."</td>
 </tr>
 <tr>
  <td class='ver' style='outline-width:$thicknesspx'>$lhook</td>
  <td class='orang' style='outline-width:$thicknesspx'>$rhook</td>
 </tr>
 <tr>
  <td class='jaune right' align=right style='vertical-align:top;'><div style='vertical-align:$valign;$flex'>$lleft".left($node,$left[$node])."</div></td>
  <td class='vert' align=left style='vertical-align:top'>$lright<div style='vertical-align:$valign;$flex'>".right($node,$right[$node])."</div></td>
 </tr>";
   // }
    if (isset($relations[$node]) and $depth>0)
        {
        //echo "depth=$depth<br>";
        $depth--;
        $r.=sons($node,$relations[$node],$depth);
        }
    else 
        $stock[]=$node; // the node to explore the sons 
    if (!$onlylines)
        $r="<table class='$class orange'>$r</table>";
    return $r;
    }
    
function liaisons(){
    global $link;
    $result='';
    foreach ($link as $key => $v)
        {
        if ($v[2]==0)
            $options="{color:'red',dash:false,endPlug:'behind'}";
        else
            $options="{color:'red',dash:true,endPlug:'behind'}";
        $result.="new LeaderLine(document.getElementById('$v[0]'),document.getElementById('$v[1]'),$options);\n";
        }
    return "\n<script> \n$result</script>";
    }
    
function woc($data,$depthmax=1000,$profile='default'){ 
    global $top,$stock;
    $stock=[];
    $css_profile=style_profile($profile);
    if (is_string($data)) // case of filename
        get_data(file_get_contents("data/$data"));
    else // $content itseft in an array
        get_data($data[0]);
    $depth=100000;//$depthmax; // decrease in chart if there are sons
    return "$css_profile<div class='woc a4l'>".chart('',$top,'','','','',$depth).'</div><br style="line-height:0%">';
}


