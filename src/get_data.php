<?php
// parsing data file

// generate a random list in '$liste' for tests
function generate($father='//top/',$depth=1,$width=7,$widthl=3,$widthr=3,$max_depth=3){
    global $liste,$profile_list;
    $sons=[];
    //view($profile_list);
    if ($depth>$max_depth){
        return;}
    for ($i=0;$i<mt_rand(1,$width);$i++){ // sons below
        $son=aleaf(uniqid());
        $sons[]=$son;
        $liste.="$son -- $father\n";
        }
    for ($i=0;$i<mt_rand(1,$widthl);$i++){ // sons left
        $son=aleaf(uniqid());
        $sons[]=$son;
        $liste.="$son -l $father\n";
        }
    for ($i=0;$i<mt_rand(1,$widthr);$i++){ // sons right
        $son=aleaf(uniqid());
        $sons[]=$son;
        $liste.="$son -r $father\n";
        }
    foreach ($sons as $son)
        generate($son,$depth+1,$width,$widthl,$widthr,$max_depth);
    }

function reduce_tree($tree,$depthmax,$depth=0){ // reduce the depth of the given tree, return result
    echo "$depth $depthmax<br>";
        if (!is_array($tree))
                $result=$tree;
        else
            foreach ($tree as $key => $son){
                    if ($depth<$depthmax)
                        $result[$key]=reduce_tree($son,$depthmax,$depth+1);
                    }
    return $result;
    }
    
    function subordinates($element,$stack){
        global $cat,$def,$relations,$left,$right;
        if (is_array($element))
            foreach($element as $key => $value)
                subordinates($key,array_merge($stack,$element));
        else{
            $elt=$element;
            if (isset($def[$element]))
                $elt=$def[$element];
            $hiscat=explode('/',$elt)[3];
            foreach($stack as $s)
                $cat[$s][$hiscat]++;
            $sons=[];
            $sons=array_merge($relations[$element]);
            if (isset($left[$element]))
                $sons=array_merge($sons,$left[$element]);
            if (isset($right[$element]))
                $sons=array_merge($sons,$right[$element]);
            foreach($sons as $son){
                subordinates($son,array_merge($stack,[$element]));
                }
            }
        }
        
    function find_place(&$item,$key,&$element){
        if(($item)==$element[0])
            $item=[$item => $element[1]];
        }
        
    function store($type,$m,$s){
        global $relations,$left,$right,$lateralleft,$lateralright,$under,$thick,$link;
        //echo "$m $type $s<br>";
        (strtoupper($type)==$type) ? $thick["$m--$s"]='':$thick["$m--$s"]='dotted';
        switch (strtoupper($type)) {
           case 'L':$lateralleft[$m]=$s;break;// only one
           case 'R':$lateralright[$m]=$s;break; // only one
           case 'A':$left[$m][]=$s;break;
           case 'B':$right[$m][]=$s;break;
           case 'N':$relations[$m][]=$s;break;
           case 'U':$relations[$m][]=$s;$under["$m$s"]=1;break;
           default: $link[]=[$m,$s,$type]; //freelinks;
            }
    }
        
    function get_data($list){
        // data list is a lit of lines like this:
        // ([token =] status/role/name/category)|token --|-t|-r|-0|-1 ([token =] status/role/name/category)|token
        global $cat,$left,$right,$lateralleft,$lateralright,$relations,$link,$top,$def,$lateral_links,$dotted,$liste,$thick;
        $liste=[];
        $relations=[];
        $left=[];
        $right=[];
        $lateralleft=[];
        $lateralright=[];
        $thick=[];
        $liste=str_replace("\r\n","\n",$list);
        $liste=explode("\n",$liste);
        preg_match('/^(.*) -(.) (.*)$/',$liste[0],$m);
        $top=$m[3];
        $n=0;
        foreach($liste as $key => &$value){
            if ($value[0]!='#' and trim($value)!=''){
                $value=trim($value);
                $value=str_replace('  ',' ',$value); // double space
                if (!$lateral_links)
                    $value=str_replace([' -r ',' -l ',' -b ',' -a ',' -R ',' -L ',' -A ',' -B ']
                                    ,[' -n ',' -n ',' -n ',' -n ',' -N ',' -N ',' -N ',' -N '],$value); // no lateral links
                
                preg_match('|^(.)( -. )(.)$|',$value,$m); // for tests
                if ($m!=[]){
                    $def[$m[1]]=aleaf($m[1]);
                    $def[$m[3]]=aleaf($m[3]);
                    }
                preg_match('/^(.*) = (.*)( -. )(.*)$/',$value,$m); // token = definition -- 
                if ($m!=[]){
                    $def[$m[1]]=$m[2]; 
                    $value=$m[1].$m[3].$m[4];
                    }
                preg_match('/^(.*)( -. )(.*) = (.*)$/',$value,$m); // -- token = definition 
                if ($m!=[]){
                    $def[$m[3]]=$m[4];
                    $value=str_replace(' = '.$m[4],'',$value);
                    if ($n==0);
                        $top=$m[3]; // case of top is a def =
                    }
                //echo "<b>$value</b><br>";
                preg_match('/^(.*) -(.) (.*)$/',$value,$m);
                if ($m!=[])
                    store($m[2],$m[3],$m[1]);
                else
                    echo "syntax error <b>$value</b><br>";
                $n++;
                }
            }
        foreach($relations as $sub => $man)
        array_walk_recursive($relations,'find_place',[$sub,$man]); // place the table in depth
        subordinates($top,[]);
        //view($relations);
        //$relations=reduce_tree($relations[$top],2);
        //view($relations,'reduced');
        return $relations;
    }
    
?>
