<?php

// module de traduction rapide pour quelques chaines sans passer par une usine à gaz

$traduction=[
  'FR'=>[
    'Theme editor'=>'Éditeur de thème',
    'Config editor'=>'Éditeur de configuration',
    'Theme to use'=>'Thème à utiliser',
    'Theme to edit'=>'Thème à éditer',
    'Debug mode active'=>'Mode déboggage activé',
    'Relations'=>'Relations',
    'Disposition'=>'Disposition',
    'Pagination'=>'Pagination',
    'Export also in pdf'=>'Exporter aussi en pdf',
    'Org chart'=>'Organigramme', 
    'Table of content'=>'Table des matières',
    'Org chart produced with WOC'=>'Organigramme produit avec WOC',
    ],
];

session_start();

function __($x){ 
  global $traduction,$_SESSION;
  if (isset($traduction[$_SESSION['lang']][$x]))
    $x=$traduction[$_SESSION['lang']][$x];
  else
    $x="***$x***";
  return $x;
  }


?>
