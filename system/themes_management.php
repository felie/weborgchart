<?php

// theme management


if (isset($_POST['theme']));

function formTest($orga='default',$pagins='default',$dispos='default'){
    return "
<form method='POST'>
 <table>
  <tr>
   <td>".
select_theme(_('to use'),0)."<br>

<b>"._('Relations')."</b><br>
<input type='hidden' name='orga' value='$orga'>
<textarea name='relations' cols=80 rows=30>".file_get_contents("orgas/$orga")."</textarea><br>

<b>"._('Arrangement')."</b><br>
<input type='hidden' name='dispos' value='$dispos'>
<textarea name='thedispos' cols=80 rows=3>".file_get_contents("dispos/$dispos")."</textarea><br>

<b>"._('Pagination')."</b><br>
<input type='hidden' name='pagins' value='$pagins'>
<textarea name='thepagins' cols=80 rows=3>".file_get_contents("pagins/$pagins")."</textarea><br>
<input type='checkbox' name='pdf'><label for='pdf'><b>"._('export also in pdf')."</b></label>
    <br><input style='vertical-align:top' type='submit' class='button' name='submit' value='"._('Draw')."'>
   </td>
  </tr>
 </table><p/><br>
</form>";
}

function aleaf($c){
    global $theme_list,$aleaLevelForLetters;
    if ($aleaLevelForLetters)
        $p=$theme_list[mt_rand(0,11)];
    else $p='default';
    $r="$p/$p/$c/".chr(mt_rand(65,67));
    //echo "$r</br>";
    return $r;
    }


function demo($x,$n){
    return "<div id=demo$n style='overflow:hidden;margin-right:auto;min-width:170px;text-align:center;padding:0px;margin-bottom:0px;border:1px solid #000;background-color:$x[1];color:$x[2];font-size:$x[3]px;width:$x[4]px;height:$x[5]px'>$x[0]</div>";
    }

    
function style_theme($theme='default'){
    global $default_level,$theme_font_size,$bloc_level,$labelwidth,$labelheight,$theme_width,$theme_height,$theme_list,$variablewidth,$labelwidth;
    $theme_list=[];
    $theme_height=[];
    $theme_width=[];
    $theme_font_size=[];
    $p=file_get_contents("themes/$theme");
    $p=explode("\n","$bloc_level\n$default_level\n$p"); // default level possibly surcharged after... security to have a default level!
    array_pop($p);
    foreach ($p as &$s){
        $s=explode(';',$s);
        foreach ($s as &$e)
            if ($e=='') $e=0;
        if ($variablewidth==0)
            $s[4]=$labelwidth;
        $s[4]=max($s[4],$labelwidth);
        if ($s[0]=='bloc')
            $s[5]=-$labelheight; // tricks to obtain 0 for bloc!
        $r.=".$s[0]_level { overflow:hidden;text-overflow:clip;background-color:$s[1];color:$s[2];font-size:$s[3]px;width:$s[4]px;margin-left:".(floor($s[4]/-2))."px;min-height:".($s[5]+$labelheight)."px;}\n";
        $theme_font_size[$s[0]]=$s[3];
        $theme_width[$s[0]]=$s[4];
        $theme_height[$s[0]]=$s[5];
        $theme_list[]=$s[0];
    }
    //view($theme_width);
    //view($theme_height);
    return "\n\n<!-- theme: $theme -->\n<style>\n$r</style>\n\n";
}

function theme_update($theme){
    $p=file_get_contents("themes/$theme"); // file to str
    if ($p=='')
        $p=str_repeat("\n",12);
    $r='';
    $r.="UPDATE <b>$theme</b> theme<form style='background-color:lightblue' method='POST'><table>";
    $p=explode("\n",$p); // str to array
    array_pop($p);
    $r.="<tr style='text-align:center'><td>statut</td><td>back</td><td>text</td><td>font</td><td>width</td><td>height</td><td>demo</td></tr>";
    $n=0;
    foreach ($p as $l)
        {
        $n++;
        $l=explode(";",$l);
        $r.="
        <tr>
         <td><input onchange='update_status($n,value)'; size=10 name=s[] value='$l[0]'></td>
         <td><input onchange='update_bc($n,value)'; type=color name=b[] value='$l[1]'></td>
         <td><input onchange='update_c($n,value)'; type=color name=p[] value='$l[2]'></td>
         <td><input onchange='update_font($n,value)'; size=2 name=f[] value='$l[3]'></td>
         <td><input onchange='update_width($n,value)'; size=3 name=w[] value='$l[4]'></td>
         <td><input onchange='update_height($n,value)'; size=3 name=h[] value='$l[5]'></td>
         <td>".demo($l,$n)."</td>
         </tr>
         ";
        }
    $r.="</table><input type=hidden name=theme value='$theme'><input type=submit name='edit' value='OK'></form>";
return $r;
}

function select_theme($s,$withform=1){
    $r='';
    $scandir = scandir("./themes");
    $scandir = array_diff($scandir, array('..', '.'));
    if ($withform)
        $r.="<form method=POST>";
    $r.="<b>"._('Theme')." $s: </b><select name='theme'>";
    foreach($scandir as $fichier){
        $r.="<option>$fichier";
        }
    $r.='</select>';
    if ($withform)
        $r.='<input type=submit value="OK"></form>';
    return $r;
    }

function new_theme(){
    $r.="<form method='POST'>New theme: <input name='new_theme'><input type=submit value='OK'></form>";
    return $r;
    }
    
function def_js(){ // for theme editor
    return "
<script>
function update_status(n,x){
    document.getElementById('demo'+n).innerHTML=x;
    }
function update_bc(n,x){
    document.getElementById('demo'+n).style.backgroundColor=x;
    }
function update_c(n,x){
    document.getElementById('demo'+n).style.color=x;
    }
function update_font(n,x){
    document.getElementById('demo'+n).style.fontSize=x+'px';
    }
function update_width(n,x){
    document.getElementById('demo'+n).style.width=x+'px';
    }
function update_height(n,x){
    document.getElementById('demo'+n).style.height=x+'px';
    }
</script>
";
}

echo def_js();

?>
