<?php

function style(){
    global $debug,$halfLabel,$halfLabelMarged,$labelWidth,$labelfontsize,$labelheight,$shadow,$thickness,$thicknesspx,$backgroundcolor,$labelbackgroundcolor,$labelpencolor,$Margin,$Marginpx,$flex,$roundborder,$oklateral;
    $thicknesspx=($thickness*2).'px';
    $oklateral=0;
$r.="
<style>

.copyleft {
  display:inline-block;
  -webkit-transform: rotate(180deg);
  -webkit-transform-origin: center bottom auto;
}

a {
    text-decoration:none;
}

.vertical-text {
    position: fixed;
    -webkit-transform: rotate(-90deg);
    -webkit-transform-origin: center bottom auto;
    right:-190px;
}

.miniature{
    -webkit-transform: rotate(-15deg);
    -webkit-transform-origin: center bottom auto;
    -webkit-backface-visibility: hidden;
    }

.button {
  text-transform: uppercase;
  padding: 5px;
  color: blue;
  font-weight: 700;
}

.cat {
  z-index: 1;
  position:absolute;
  overflow: visible;
  bottom: -16px;
  font-weight: bold;
  background-color: white;
  color: black;
}

  .catA {
  left:5px;
  }
  
  .catB {
  left: 25px;
  }

  .catC {
  left: 45px;
  }

  .hiscat{
  right: 10px;
  }


.label:hover{ background:black; color:ivory }
  
.label {   
  text-align:center;
  position:static;
  overflow:visible;
  text-overflow:clip;
  font-size:$labelfontsize"."px;
  /*min-height:$labelheight"."px;*/
  min-width:$labelWidth;
  background-color:$labelbackgroundcolor;
  color:$labelpencolor;
}
    
body {margin-left:$halfLabelMarged;margin:0px;padding:0px}
table {margin:auto}
.woc {
    word-break: break-word;
    background-color:$backgroundcolor;
    /*padding:".floor($Margin/2)."px;*/
    padding-bottom:".($Margin*2)."px;
    padding-top:0px;
    /*border:1px solid gray;*/
    }
table,tr,td {
    border-spacing:0;
    border-collapse:collapse;
    border:0px;
    margin:0px;
    padding:0px;
    }
.a4l {
    /*background-color:lightgray;*/
    min-width:631.4pt;
    min-height:700.4pt;
    margin: 0 0 0 0;
    padding: 0 0 0 0;
    z-index:-1000;
}
.a4p {
    /*background-color:lightgray;*/
    width:446.4pt;
    min-height:631.4pt;
    margin: 0 0 0 0;
    padding: 0 0 0 0;
    z-index:-1000;
}

/* new page for wkhtmltopdf */ 

.keep-together {
    page-break-inside: avoid;
}

.break-before {
    page-break-before: always;
}

.break-after {
    page-break-after: always;
}

.cover{
    font-size:72px;
    text-align:center;
    width:1700pt;
    /*rder:3px solid black;*/
    margin-top:100px;
    /*height:800pt;*/
    /*vertical-align:middle;*/
}

@page { /* for weadyprint see http://test.weasyprint.org/suite-css-page-3/chapter5/section1/test1/ */
    size:landscape;	
    }
    
.right {border-right:$thicknesspx solid black}
.rightdotted {border-right:$thicknesspx dotted black}
.rightb {border-right:$thicknesspx solid blue}
.bottom {border-bottom:$thicknesspx solid black}
.bottomdotted {border-bottom:$thicknesspx dotted black}
.left {border-left:$thicknesspx solid black}
.leftdotted {border-left:$thicknesspx dotted black}
.top {border-top:$thicknesspx solid black}
.topdotted {border-top:$thicknesspx dotted black}";

if ($flex)
    $flex="display:flex;flex-grow:1;flex-direction:column";
    
if ($roundborder!=0){
    $x=$roundborder.'px';
    $r.=".radius1 { 
    border-radius:$x $x $x $x;
    }
.radius2 {
    border-radius:0 0 $x $x;
    }";
    }

if ($debug)
    $r.="
table,td,tr,th { border:1px dotted gray;margin:5px}
.machin td {width:100px;height:50px}
.rose {background-color:#FFCCFF;}
.bleu {background-color:#CCFFFF;}
.vert {background-color:#CCFFCC;}
.orange {background-color:#FFCCCC;}bolor:#CCCCCC;}
.test {border:10px solid red}
.rightr {border-right:$thicknesspx solid red}
.leftr {border-left:$thicknesspx solid red}
.rightb {border-right:$line solid blue}
.leftb {border-left:$thicknesspx solid blue}
";
$r.="
</style>";
return $r;
}
?>
