<?php
/*
    Licenced GPLv2
    francois@elie.org 2022 
*/
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED);
ini_set("display_errors", 1);

include_once 'get_data.php';
include_once 'themes_management.php';
include_once 'style.php';
include_once 'util.php';


if ($debug)
    echo "<h1>"._('debug mode active')."</h1>";

$left=[];
$right=[];
$def=[];
$relations=[];
$link=[];
$cat=[];

    function cats($c,$hiscat){
        global $category_in_label;
        $r='';
        if (!isset($c['A']))
            $c['A']=0;
        if (!isset($c['B']))
            $c['B']=0;
        if (!isset($c['C']))
            $c['C']=0;
        $r='';
        if ($c['A']+$c['B']+$c['C']>0)
            $r.= "<div class='cat catA'>".$c['A']."</div><div class='cat catB'>".$c['B']."</div><div class='cat catC'>".$c['C']."</div>";
        if ($category_in_label and $hiscat!='')
            $r.="<div class='cat hiscat'>$hiscat</div>";
        return $r;
        }
        
// version 2

$shadows='';
if ($shadow)
$shadows="-webkit-box-shadow: 2px 2px 11px 1px #000000; 
box-shadow: 2px 2px 11px 1px #000000;";

function alea(){
    return mt_rand(10,90).'px ';
    }
    
function trueStr($s){
    global $def;
    if (isset($def[$s]))
        $s=$def[$s];
    return $s;
    }
    
function nameOf($s){
    $s=explode('/',trueStr($s));
    $s=trim($s[2]);
    if ($s=='')
        $s='x';
    return $s;
    }
    
function levelOf($s){
    $s=explode('/',trueStr($s));
    $s=trim($s[0]);
    if ($s=='')
        $s='default';
    return $s;
    }
    
function serviceOf($s){ // "The Sample Company(ceo)" return "The Sample Company"
    $s=explode('/',trueStr($s));
    $s=explode('(',$s[1]);
    return $s[0];
    }
    
function functionOf($s){ // "The Sample Company(ceo)" return "ceo"
    $s=explode('/',trueStr($s));
    $s=explode('(',$s[1]);
    $s=explode(')',$s[count($s)-1]);
    return $s[0];
    }
    
function upcaseFirstName($s){
    $s=explode(', ',$s);
    foreach ($s as &$i){
        $i=explode(' ',$i);
        $n=0;
        foreach ($i as &$j){
            if ($n>0)
                $j=strtoupper($j);
            $n++;
            }
        $i=implode(' ',$i);
        }
    $s=implode(', ',$s);
    $s=str_replace(['é','ê'],['É','Ê'],$s);
    //echo "*$s*<br>";
    return $s;
    }

function node($from,$s)
        {
        global $def,$cat,$relations,$categories_sum,$labelWidth,$labelheight,$shadows,$aleaborder,$theme_height,$theme_width,$theme_list,$debug,$Margin,$lateral_child,$oklateral,$lateraldecal,$hook_height,$sepInBlock,$thickness,$sepInBlock,$upcasefirstname;
        $title=$s;
        if (isset($def[$s])){
            //echo "def de $s<br>";
            $id=" id='$s'"; // token is taken for id
            $s=$def[$s];
            }
        $pos=strpos($s,'/');
        if ($pos===false)
            $s="default//$s/";
        //echo "s=$s<br>";
        $t=explode('/',$s);
        if (!isset($id))
            $id=" id='$s'" ;
        $t[0]=strtolower($t[0]); // for the theme statut
        $t[0]=levelOf($s); 
        if ($t[0]=='')
            $t[0]='default';
        if ($t[1]=='')
            $t[0]='bloc'; //***
        $t[1]=str_replace(['(',')'],['<hr>',''],$t[1]);
        if ($debug)
            $t[1]=$t[0];
        $t[0].='_level';
        if ($upcasefirstname)
            $t[2]=upcaseFirstName($t[2]);
        $t[2]=str_replace(', ',"<div style='height:$sepInBlock'></div>",$t[2]); // several persons
        /*if ($t[2]=='')
            $t[2]='x';*/
        if ($categories_sum)
            $categories=cats($cat[$title],$t[3]);
        //if ($categories!='')
          //  $categories="<br>$categories";
        $hh=$theme_height[levelOf($s)];
        if (!$lateral_child or !$oklateral)
            $r.="<div $id class='$t[0] radius1' style='overflow:visible;position:relative;z-index:1;text-align:center;$shadows;'>
                    <div style='min-height:$hh"."px'>$t[1]</div>
                <div class='label radius2' style='overflow:visible;text-align:center;vertical-align:bottom;'>
                $t[2]$categories</div></div>";
                
        else {
            $w=$theme_width[levelOf($s)];
            $dw=floor($w/2);
            $r.="<div style='padding-left:$thickness"."px'><div class='left bottom' style='margin-left:-".($dw)."px'>&nbsp;<div $id class='$t[0] radius1' style='margin-bottom:-$labelheight"."px;margin-left:$lateraldecal"."px;position:relative;z-index:1;display:table;text-align:center;$shadows;'>
                    <div style='position:relative;overflow:hidden;min-height:$hh"."px'>$t[1]</div>
                <div class='label radius2' style='vertical-align:bottom;'>
                    $t[2]
                </div>
                </div>
                <div style='z-index:2'>".$categories."</div></div></div>";
            }
        return $r;
        }
        
    
$fin='0.001em';
$heightpx=$vspace.'px';
$hookHeight=$hook_height.'px';
$labelWidth=$labelwidth.'px';
$labelWidthMarged=($labelwidth+$Margin).'px';
//$label3=floor($labelwidth/2+$Margin).'px';
$halflabel=floor($labelwidth / 2);
$halfLabel=$halflabel.'px';
$Marginpx=$Margin.'px';
//$MarginLpx=($Margin+floor($labelwidth / 2)).'px';
//$halfLabelMarged=(floor($labelwidth / 2)+$Margin).'px';
function th($link){
    global $thick;
    $result='';
    if (!$thick["$link"])
        $result='dotted';
    return $r;
    }
    
function leftHook($th){
    global $hookHeight;
    return "<tr><td style='vertical-align:top;width:100%' class='right$th'></td><td class='top$th' style='width:100%;height:$hookHeight'></td></tr>";
    }
    
function simpleHook($th){
    global $hookHeight;
    //echo "$th dans simplehook <br>";
    return "<tr><td class='right$th' style='vertical-align:top$th;height:$hookHeight'></td></tr>";
    }

$noHook="<tr><td style='vertical-align:top;height:$hookHeight'></td></tr>";

function rightHook($th){
    global $hookHeight;
    return "<tr class=ver><td class='top$th right$th' style='vertical-align:top;width:100%;height:$hookHeight'></td><td></td></tr>";
    }

function topHook($th){
    global $hookHeight,$Marginpx;
    return "<tr><td class='top right$th ver' style='vertical-align:top;height:$hookHeight'><div style='padding-right:$Marginpx;'></div></td><td class='top'></td></tr>";
    }
    
function hookHook($th,$e){
    return lline($e,$th);
    }


function vspace($v,$class){
    return "<tr><td class='$class'  ><div style='height:".$v."px'></div></td><td></td></tr>";
    }

function son($node,$hook,$onlylines,$depth){
    if (is_string($node))
        return chart('',$node,$hook,'','',$onlylines,$depth);
    else
        foreach($node as $key => $value) // one array only
            return chart('',$key,$hook,'','',$onlylines,$depth);
    }
    
function sons($from,$a,$depth){
    global $left,$right,$backgroundcolor,$rightHook,$topHook,$simpleHook,$labelWidthMarged,$sepInBlock,$labelwidth,$block_mode,$compact_blocks,$halfLabel,$halfLabelMarged,$Marginpx,$line,$hook_height,$thick,$lateral_child,$oklateral;
    $bloc=true;
    if ($block_mode){
        foreach ($a as $n){
            if (!is_string($n))
                $bloc=false;
            else
                if (isset($left[$n]) or isset($right[$n]))
                    $bloc=false;
                }
            }
    else
        $bloc=false;
    $r=[];
    $n=0;
    if ($bloc){
        if ($lateral_child and !isset($left[$from]) and !isset($right[$from]))
            $oklateral=1;
        foreach ($a as $e){
            if ($compact_blocks)
                $r[]=str_replace("'",'&#39;',nameOf($e)); // then symbol ' break css instructions
            else
                $r[]=chart($from,$e,'','','',1,$depth);
            if ($n==0)
                $first=$e;
            $n++;
            }
        if ($compact_blocks)
            {
            $e='bloc//'.implode("<hr style=\"border: none; margin-top:1px;margin-bottom:1px;border-bottom: 1px solid black;\">",$r).'/';
            
            $r=[chart($from,$e,'','','',1,$depth)];
            }
        //$r=implode("<br><div style='margin-bottom:".(-20+$sepInBlock)."px'></div>",$r);
        if (!$oklateral)
            $r=vspace($hook_height,'right'.$thick["$from--$first"]).implode(vspace($sepInBlock,''),$r);
        else
            $r=implode('',$r);
        $oklateral=0;
        return "<tr><td></td><td>$r</td></tr>";
        }
    else{
        $nb=sizeof($a);
        $lefthand=floor($nb/2);
        $n=0;
        $r='';
        foreach ($a as $e){
            if ($nb==1){ // probably useless
                $rs=son($e,simpleHook($thick["$from--$e"]),1,$depth); // one son only
                }
            else
                if ($n==0){
                    $f=$e;
                    if (is_array($e))
                         $f=array_keys($e)[0];
                    $rs=son($e,leftHook($thick["$from--$f"]),0,$depth);
                    $first=$e;
                    }
                else
                    if ($n==($nb-1)){
                        $f=$e;
                        if (is_array($e))
                            $f=array_keys($e)[0];
                        $rs=son($e,rightHook($thick["$from--$f"]),0,$depth);
                        }
                    else
                        $rs=son($e,topHook($thick["$from--$e"]),0,$depth);
            if ($nb==1)
                return "$line $rs"; // case of on son only
            $rs="<td>$rs</td>";
            $n++;
            $r.=$rs;
            if ($n==$lefthand) // $trick to be sure the father is connected to rake (the line is divides in two table-columns)
                $r="$r
  </tr>
 </table>
</td>
<td align=left style='vertical-align:top;'>
  <table class=gree>
  <tr style='vertical-align:top;'>";
            }
        $r=
        vspace($hook_height,'right')."
 <tr class=orang><td align=right style='vertical-align:top;'>
 <table class=blu>
 <tr style='vertical-align:top;'>$r
 </tr>
 </table></td></tr>";
        }
    return $r;
    }

function correction_height($node){
    global $theme_height,$labelheight;
    return floor(($theme_height[levelOf($node)]+$labelheight)/2);
    }
    
function left($from,$a,$depth){
    global $noHook,$thick;
    if ($a=='')
        return '';
    $r=[];
    foreach ($a as $key => $value)
        $r[]=chart($from,$value,$noHook,'',lline($value,$thick["$from--$value"]),0,$depth);
    return implode($r);
    }
    
function right($from,$a,$depth){
    global $noHook,$thick;
    if ($a=='')
        return '';
    $r=[];
    foreach ($a as $key => $value)
        $r[]=chart($from,$value,$noHook,lline($value,$thick["$from--$value"]),'',0,$depth);
    return implode($r);
    }

define('hspace',"<div style='margin-top:-10px;height:10px;margin-left:0'></div>");

function lline($node,$th,$position='relative'){
    global $thickness,$thick;
    if ($th=='')
        $th='solid';
    return "<div style='position:$position;margin-top:-".correction_height($node)."px;height:0px;overflow:hidden;z-index:0;outline:".$thickness."px $th black;width:100%'></div>";
    }

function hspace($node,$side){
    global $theme_width,$Marginpx;
    $w=floor($theme_width[levelOf($node)]/2)+$Margin.'px';
    if ($side=='left')
        $p="padding-right:$Marginpx";
    if ($side=='right')
        $p="padding-left:$Marginpx";
    return "style='vertical-align:top;margin-top:-10px;$p;height:10px;min-width:$w'";
    }

define('vspace',"<tr><td style='height:$vspace"."px'></td><td></td></tr>");

 function chart($from,$node,$hook=vspace,$lhook='',$rhook='',$onlylines=0,$depth=0){
    global $item,$relations,$lateral,$left,$right,$noHook,$leftHook,$rightHook,$lateralleft,$lateralright,$halfLabelMarged,$Margin,$Marginpx,$labelheight,$hook_height,$thicknesspx,$backgroundcolor,$line,$valign,$theme_width,$theme_height,$flex,$thick,$oklateral,$stock;
    $item[]=$node; // item in ordre of the tree! for the table of content
    //echo "depth=$depth<br>";
    $p=levelOf($node);
    $pw=floor($theme_width[$p]);
    $h=floor($theme_height[$p]);
    $dw=floor($pw/2);
    $w=($dw+$Margin).'px';
    $w2=($dw+4*$Margin).'px';
    $r='';
    if ($depth>1){ // if last layer: no lateral childs
        if (isset($lateralleft[$node]))
            $lleft="<div style='margin-top:-".($theme_height[levelOf($lateralleft[$node])]+2*$labelheight)."px;padding-right:$dw"."px'>".chart($from,$lateralleft[$node],'','',lline($lateralleft[$node],$thick["$node--".$lateralleft[$node]]),0,$depth)."</div><br>";
        if (isset($lateralright[$node]))
            $lright="<div style='margin-top:-".($theme_height[levelOf($lateralright[$node])]+2*$labelheight)."px;padding-left:$dw"."px'>".chart($from,$lateralright[$node],'',lline($lateralright[$node],$thick["$node--".$lateralright[$node]]),'',0,$depth)."</div><br>";
        if (!$oklateral)
            $classright='right';
        $bleft=left($node,$left[$node],$depth);
        $bright=right($node,$right[$node],$depth);
        }
    $r.="
        $hook
        <tr style='vertical-align:top'>
        <td class='rose $classright' align=right ".hspace($node,'right')."></td>
        <td class='bleu' ".hspace($node,'left').">".node($from,$node)."</td>
        </tr>
        <tr>
        <td class='ver' style='outline-width:$thicknesspx'>$lhook</td>
        <td class='orang' style='outline-width:$thicknesspx'>$rhook</td>
        </tr>
        <tr>
        <td class='jaune right' align=right style='vertical-align:top;'><div style='vertical-align:$valign;$flex'>$lleft $bleft</div></td>
        <td class='vert' align=left style='vertical-align:top'>$lright<div style='vertical-align:$valign;$flex'> $bright</div></td>
        </tr>";
   // }
    if (isset($relations[$node]) and $depth>1)
        {
        //echo "depth=$depth<br>";
        $depth--;
        $r.=sons($node,$relations[$node],$depth);
        }
    else 
        if (!in_array($node,$lateral) and isset($relations[$node])) 
            $stock[]=$node; // the node to explore the sons for the another charts
    if (!$onlylines)
        $r="<table class='$class orange'>$r</table>";
    return $r;
    }
    
function liaisons(){
    global $liaisons,$link;
    if ($liaisons==0)
        return;
    $result='';
    foreach ($link as $key => $v)
        {
        if ($v[2]==0)
            $options="{color:'red',dash:false,endPlug:'behind'}";
        else
            $options="{color:'red',dash:true,endPlug:'behind'}";
        $result.="new LeaderLine(document.getElementById('$v[0]'),document.getElementById('$v[1]'),$options);\n";
        }
    return "\n<script> \n$result</script>";
    }
    
function woc($data,$depthmax=-1,$theme='default'){ 
    // 3 cases. data is a string beginning by "orgas/": its a file, an array, or string (a node in $relation)
    global $relations,$top;
    if ($depthmax==-1) // -1 = infinite
        $depthmax=10000;
    if ($depthmax==0)
        return 'depth=0 ???<br>';
    $css_theme=style_theme($theme);
    if (is_string($data)){
        if (substr($data,0,6)=='orgas/'){ // case of a filename 
            get_data(file_get_contents($data));
            $node=$top;
            }
        else // cas of a simple node
            $node=$data;
        }
    else{ // $content itseft in an array
        get_data($data[0]);
        $node=$top;
        }
    return "$css_theme<div class='woc a4l'>".chart('',$node,'','','','',$depthmax).'</div><br style="line-height:0%">'.liaisons();
    }
    
$header_page="<!DOCTYPE html>\n<html lang='fr'>\n <head>\n  <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>";

function storeInPage($x,$n){
    $anchor='<a name="page'.sprintf("%03d",$n)."\">&nbsp;</a>"; // empty a ???
    return $anchor.$x.
    '<div class="vertical-text"><small>'._('Orgchart produces with').' WOC <span class="copyleft">&copy;</span> Elie 2022 GPLv3 Affero</small></div>
    <div style = "display:block; clear:both; page-break-after:always;"></div>'; // return anchor+page+pagebreak;
    }
    
function storeInDocument($d,$t){ // $t is a $table
    global $header_page,$orientation,$produce_pdf;
    $t=implode('',$t); // $t as str
    file_put_contents("temp/$d.html",$header_page.style().$t);
    if ($produce_pdf){
        if ($orientation=='a4l')
            $option=' --orientation landscape ';
        exec("cd temp;wkhtmltopdf $option $d.html $d.pdf;sed -i 's|file:///var/www/html/woc/temp/result.html#page|#page=|' result.pdf"); 
        }
    }
    
function deleteAllpages(){
    exec("cd temp;rm *pdf");
    }

function tocLine($node,$u){
    global $flip,$theme_font_size;
    $nlevel=$flip[levelOf($node)];
    return str_repeat('&nbsp;',3*$nlevel)." <a name='ret$u'></a><a href='#$u'><span style='font-size:".$theme_font_size[levelOf($node)]."px;'>".functionOf($node)."</span></a><br>";
    }
    
function addPage($node,$depth){
    global $tocLine,$pg;
    $u=uniqid();
    $tocLine[$node]=tocLine($node,$u);
    $pg[$node]="<a name='$u'></a><h1>".serviceOf($node)." <a href='#ret$u'>↩</a></h1>".woc($node,$depth);
    }
    
function cover($deco){
    global $top;
    return "<table class=cover><tr><td><b>".serviceOf($top)."</b><br>"._('Orgchart')."<br></td></td></tr><tr><td></td></tr></table><br><br><br><br>$deco";
    }
    
// usse order of the form "level -> <letter>" as "assistant -> L"
function modify_disposition($orga,$dispo){
    $dispo=explode("\n",file_get_contents("dispos/$dispo")); // orders of disposition
    foreach ($dispo as $d){
        $d=explode(' -> ',$d);
        $d[1]=trim($d[1]);
        $search="/^(.* = )*$d[0](.*) -. (.*)$/m";
        $orga=preg_replace($search,"$1$d[0]$2 -$d[1] $3",$orga);
        }
    return $orga;
    }
    
function HTML($orga,$dispo,$pagin){
    global $orientation,$tocLine,$pg,$flip,$orga,$top,$item,$theme_list,$produce_pdf,$cover;
    /* three possibles pagins[top|a node|a level*]->depth */
    $orga=file_get_contents("orgas/$orga"); // list of relations
    $orga=modify_disposition($orga,$dispo);
    get_data($orga); // fill $relations etc...
    // transformation of relation by orders in "disposition file" on $relations
    $deco="<div class='miniature'>".woc($top)."</div>"; 
    $pagin=file_get_contents("pagins/$pagin");
    $cover=cover($deco);
    $pagin=explode("\n",$pagin);
    $it=$item; // woc modify $item!
    $flip=array_flip($theme_list); // value => key
    foreach($pagin as $o){
        //echo "order <b>$o</b><br>";
        $x=explode('->',$o);
        if ($x[0]=='top')
            addPage($top,$x[1]);
        else
            if ($x[0][strlen($x[0])-1]=='*'){
                $f=substr($x[0],0,-1);
                //echo "f=$f<br>";
                foreach($it as $i)
                    if (levelOf($i)==$f)
                        addPage($i,$x[1]);
                }
            else
                addPage($x[0],$s[1]);
        }
    if ($produce_pdf){
        $pdf[1]=storeInPage($cover,1);
        }
    $n=3; // 1 is the cover, 2 the doc
    foreach($it as $i){
        if (isset($pg[$i])){
            $result.=$pg[$i].'<hr>';
            if ($produce_pdf)
                $pdf[$n]=storeInPage($pg[$i],$n);
            $toc.=$tocLine[$i];
            $n++;
            }
        }
    $toc='<h1>'._('Table of contents').'</h1>'.$toc;
    if ($produce_pdf){
        $pdf[2]=storeInPage($toc,2);
        ksort($pdf);
        storeInDocument('result',$pdf);
        }
    return "<div class='$orientation'>$cover</div><hr><div class='$orientation'>$toc</div><hr>$result";
    }
