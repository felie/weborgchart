# Relations

- **--** ou **-N**
    
>    direction/Executive director/Donald/B -- top/Financial director/Picsou/A
    
    $lateral_child=0; (default)

![img](./images/N1.png "simple 1")

- variante

>    direction/Executive director/Donald/B -- top/Financial director/Picsou/A
    
    $lateral_child=1;

![img](./images/N2.png "simple 2")

- **-L**

>    direction/Executive director/Donald/B -L top/Financial director/Picsou/A

![img](./images/L.png "left")

- **-R**

>    direction/Executive director/Donald/B -R top/Financial director/Picsou/A

![img](./images/R.png "right")

- **-A**

>    direction/Executive director/Donald/B -A top/Financial director/Picsou/A

![img](./images/A.png "A")

- **-B**

>    direction/Executive director/Donald/B -B top/Financial director/Picsou/A

![img](./images/B.png "B")

- **-b**

dotted lines are done with lowcase letters

>    direction/Executive director/Donald/B -b top/Financial director/Picsou/A
    
![img](./images/b.png "b")
    
# Blocs vs rateaux

Avec ceci:

    B -- A
    C -- B
    D -- B
    E -- D
    F -- D
    G -- B
    H -- A
    I -- H
    J -- H
    K -- H

WOC produit ceci:

![img](./images/blocvsrake.png "Default")

Remarquez que si le rateau n'est pas nécessaire, le système dessine des blocs, pour éviter des organigrammes inutilement larges
