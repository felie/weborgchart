<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!--<link type="text/css" rel="stylesheet" href="src/style.css"/>-->
    <script src="system/anseki-leader-line/leader-line.min.js"></script>
</head>
<body> 
<?php
$debut=microtime(true);

error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED);
ini_set("display_errors", 1);
    
include_once 'config.php';
session_start();
echo session_id();
include_once 'i18n.php';
include_once 'system/woc.php';
echo style();

//view($_POST);

$orga='demo';
$pagin='demo';
$dispo='demo';
$produce_pdf=1;
$debug=0;

if (isset($_POST['submit'])){
    file_put_contents('orgas/'.$_POST['orga'],$_POST['relations']);
    file_put_contents('pagins/'.$_POST['pagins'],$_POST['thepagins']);
    file_put_contents('dispos/'.$_POST['dispos'],$_POST['thedispos']);
    $produce_pdf=($_POST['pdf']=='on');
        
    //echo 'orgas/'.$_POST['file'].' '.$_POST['relations'];
    }

echo "<form action='theme_manager.php' target=_blank>
<input type='submit' value='"._('themes manager')."' />
</form>";
$theme='default';
if (isset($_POST['theme']))
    $theme=$_POST['theme'];
echo _('Active theme').": <b>$theme</b><hr/>";

echo "<div style='float:right'>".formTest($orga)."</div><br>";    

if ($produce_pdf)
    echo "<iframe class=$orientation src='https://elie.org/woc/temp/result.pdf#view=fit'></iframe>";

echo HTML($orga,$dispo,$pagin);

$fin = microtime(true);
$duree=$fin-$debut;
echo "<hr>".sprintf(_('Run time: %s seconds'),$duree);
?>

</body>
</html>
